@extends('layout/main')

@section('title', 'data mahasiswa')
    

@section('container')
<div class="container">
  <div class="row">
    <div class="col-10">
      <h1 class="mt-02">Data Dengan Perulangan Foreach</h1>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">NIM</th>
                    <th scope="col">JURUSAN</th>
                </tr>
            </thead>
            @foreach ($datas as $data)
            <tbody>
                <tr>
                    <td>{{ $data['nama'] }}</td>
                    <td>{{ $data['nim'] }}</td>
                    <td>{{ $data['jurusan'] }}</td>
                </tr>
            </tbody>
            @endforeach
        </table>
        <h1 class="mt-02">Data Dengan Perulangan For</h1>
          @for($i = 0; $i < 5; $i++)
          <p>The Value of i is {{ $i }}</p>
          @endfor
    </div>
  </div>
</div>

@endsection